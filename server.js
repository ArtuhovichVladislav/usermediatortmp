const express = require("express");
const app = express();
const spawn = require("child_process").spawn;
const fs = require("fs");

app.use(express.static("static"));
const server = require("https").createServer(
  {
    key: fs.readFileSync("server.key"),
    cert: fs.readFileSync("server.cert")
  },
  app
);

const io = require("socket.io")(server);

let ffmpegProcess;
let feedStream = false;

spawn("ffmpeg", ["-h"]).on("error", function(m) {
  console.error(
    "FFMpeg not found in system cli; please install ffmpeg properly or make a softlink to ./!"
  );
  process.exit(-1);
});

io.on("connection", function(socket) {
  socket.emit("message", "Hello from mediarecorder-to-rtmp server!");

  socket.on("configure_rtmp", function(m) {
    const regexValidator = /^rtmp:\/\/[^\s]*$/;
    if (typeof m != "string") {
      socket.emit("fatal", "rtmp destination setup error.");
      return;
    }
    if (!regexValidator.test(m)) {
      socket.emit("fatal", "rtmp address rejected.");
      return;
    }
    socket._rtmpDestination = m;
    socket.emit("message", "rtmp destination set to:" + m);
  });

  socket.on("start", function(m) {
    if (!socket._rtmpDestination) {
      socket.emit("fatal", "no destination given.");
      return;
    }

    if (!ffmpegProcess || !feedStream) {
      const ops = [
        "-i",
        "-",
        "-c:v",
        "libx264",
        "-preset",
        "veryfast",
        "-tune",
        "zerolatency", // video codec config: low latency, adaptive bitrate
        "-c:a",
        "aac",
        "-ar",
        "44100",
        "-b:a",
        "64k", // audio codec config: sampling frequency (11025, 22050, 44100), bitrate 64 kbits
        "-y", //force to overwrite
        "-use_wallclock_as_timestamps",
        "1", // used for audio sync
        "-async",
        "1", // used for audio sync
        "-bufsize",
        "1000",
        "-f",
        "flv",
        socket._rtmpDestination
      ];

      ffmpegProcess = spawn("ffmpeg", ops);

      feedStream = function(data) {
        ffmpegProcess.stdin.write(data);
      };

      ffmpegProcess.stderr.on("data", function(d) {
        socket.emit("ffmpeg_stderr", "" + d);
      });

      ffmpegProcess.on("error", function(e) {
        console.log("child process error " + e);
        socket.emit("fatal", "ffmpeg error!" + e);
        feedStream = false;
        socket.disconnect();
      });

      ffmpegProcess.on("exit", function(e) {
        console.log("child process exit " + e);
        socket.emit("fatal", "ffmpeg exit!" + e);
        socket.disconnect();
      });
    }
  });

  function stopStreaming() {
    feedStream = false;
    if (ffmpegProcess) {
      try {
        ffmpegProcess.stdin.end();
        ffmpegProcess.kill("SIGINT");
      } catch (e) {
        console.warn("killing ffmpeg process attempt failed...");
      }
    }
  }

  socket.on("binarystream", function(m) {
    if (!feedStream) {
      socket.emit("fatal", "rtmp not set yet.");
      stopStreaming();
      return;
    }
    feedStream(m);
  });

  socket.on("disconnect", function() {
    console.log('disconnect called');
    stopStreaming();
  });

  socket.on("error", function(e) {
    console.log("socket.io error:" + e);
  });

  socket.on("user_id", function(id) {
    console.log("user id is", id);

    socket.emit("start_stream_with_id", id);
  });
});

io.on("error", function(e) {
  console.log("socket.io error:" + e);
});

server.listen(3001, function() {
  console.log("https and websocket listening on *:3001");
});

process.on("uncaughtException", function(err) {
  // handle the error safely
  console.log(err);
  // Note: after client disconnect, the subprocess will cause an Error EPIPE, which can only be caught this way.
});
